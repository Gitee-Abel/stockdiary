package com.cch.platform.web;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

@ControllerAdvice
public class CommonController {

	static CustomDateTimeEditor dtEditor=new CustomDateTimeEditor(true);
	static CustomTimestampEditor ttEditor=new CustomTimestampEditor(true);
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    binder.registerCustomEditor(Date.class, dtEditor); 
		binder.registerCustomEditor(Timestamp.class,ttEditor);
	}
}
