package com.cch.stock.report.web;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cch.platform.util.DateUtil;
import com.cch.platform.web.WebUtil;
import com.cch.stock.report.bean.AssertState;
import com.cch.stock.report.bean.QueryTime;
import com.cch.stock.report.service.AssertStateService;
import com.cch.stock.report.service.MoneyReportService;

@Controller
@RequestMapping(value = "/report/assertstatet")
public class AssertStateReport {

	@Autowired
	private AssertStateService service;
	
	@RequestMapping(value = "/query.do")
	public String  query(QueryTime qt,ServletRequest request,ModelMap mm) throws ParseException {
		Map<String,List<Object>>re=service.getAssertState(WebUtil.getUser(),qt);
		mm.putAll(re);
		return "jsonView";
	}

}
