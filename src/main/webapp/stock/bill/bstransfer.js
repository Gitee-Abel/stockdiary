$(function(){
	$('#dg').edatagrid({
		url : plat.fullUrl("/bill/bstransfer/pagequery.do"),
		destroyUrl : plat.fullUrl("/bill/bstransfer/delete.do")
	});
});

function click_save(){
	var options=$('#dg').edatagrid('options');
	$('#dg').edatagrid("endEdit",options.editIndex);
	var data=$('#dg').edatagrid("getSelected");
	$.post(plat.fullUrl("/bill/bstransfer/save.do"), 
			data,
			function(){
				$('#dg').edatagrid("reload");
			}
		);
}

function click_delete(){
	$('#dg').edatagrid('destroyRow');
}

function click_upload() {
	$.upload({
		action : plat.fullUrl("/bill/bstransfer/upload.do"), //上传地址
		fileName : "file",    //文件名称。用于后台接收
		accept : ".xlsx,.xls" //文件类型
	});
}